#!/usr/bin/env bash

export PATH=$PATH:.

if ! which cli53 &>/dev/null
then
	DL=$(curl -sL https://api.github.com/repos/barnybug/cli53/releases/latest | jq -r '.assets[]|select(.name=="cli53-linux-amd64")|.browser_download_url')
	curl -sL -o cli53 $DL
	chmod +x cli53
fi

if ! cli53 list &>/dev/null
then
	echo "Failed to list zones, failed setup ?"
	exit 1
fi
