# GitOps scripts for AWS Route53

Scripts to manage AWS Route53. Originaly intended for backup & restore, then rewritten for GitOps.

## Install

Requires [cli53](https://github.com/barnybug/cli53) in the PATH or the current folder. `./install_cli53.sh` will get it for you (requires `jq`).

Requires access to your AWS Route53. Either through `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` as environment variable. Or via the default profile in `~/.aws`

## Export all

`export_all_zones.sh /path/to/storage/folder` will dump, for each available zone, all records in a DNS Zone file in the target folder.

## Import one

`import_zone.sh /path/to/storage/folder your.zone.` will import `/path/to/storage/folder/your.zone.` on `your.zone.`.

- By default proposed changes will be displayed and you'll be prompted for a confirmation.
- Use `-f` to force import.
- The first import will be very verbose due to records being sorted on export.

Exemple after removing a record:

```
➜ ~ ./import_zone.sh ./backup_20190509151417/ your.domain.
Dry-run, changes that would be made:
- server-23.your.domain.	300	IN	A	123.123.123.123

Do you want to apply those ? (Yy)n
Cancelled, nothing done.
```

## Import all

`import_zone.sh /path/to/storage/folder` will import all zone files from the given folder, guessing zone name from the file name. *No confirmation will be asked !*.

## Backup & GitOps

See respectives Jenkinsfile.

- Both requires `aws_route53_access_key_id` and `aws_route53_secret_access_key` in Jenkins' credentials
- Zone creation / destruction isn't implemented for the later workflow
