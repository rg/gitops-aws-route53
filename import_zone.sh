#!/bin/bash

usage() {
  echo "Import a DNS Zone in AWS Route53 using a DNS Zone file"
  echo
  echo "Usage: $0 [-f] /path/to/storage/dir zone.name."
  echo
  echo "Options: "
  echo "    -f    No prompt"
}

# Options
while getopts "h?f" opt; do
    case "$opt" in
    h|\?)
        usage
        exit 0
        ;;
    f)  FORCE="yes"
        ;;
    esac
done
[ "${1:-}" = "--" ] && shift

# Arguments
if [ $# -ne 2 ]
then
  usage
  exit 1
fi

# Variables
STORAGE_DIR=$1
ZONE=$2
FILE="$STORAGE_DIR/$ZONE"



# Checks
if [ ! -d $STORAGE_DIR ]
then
  echo "Storage dir '$STORAGE_DIR' doesn't exists"
  exit 2
fi

if [ ! -r "$FILE" ]
then
  echo "Can't read file $FILE."
  exit 3
fi



# Main
export PATH=$PATH:.

# Dry run and exit if no confirmation
if [ ! $FORCE == "yes" ]
then
  cli53 import --dry-run --file $FILE --replace $ZONE
  echo
  read -p "Do you want to apply those ? (Yy)" -n 1 -r
  echo
  if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
    echo "Cancelled, nothing done."
    exit 0
  fi
fi

# Import
if cli53 import --file $FILE --replace $ZONE
then
  echo "Done."
else
  echo "Something went wrong!" >&2
  exit 4
fi
