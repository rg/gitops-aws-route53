#!/bin/bash

usage() {
  echo "Import all DNS Zones in AWS Route53 using all DNS Zone file in the given folder. NO CONFIRMATION ASKED !"
  echo
  echo "Usage: $0 /path/to/storage/dir"
}

# Variables
STORAGE_DIR=$1




# Checks
if [ ! -d $STORAGE_DIR ]
then
  echo "Storage dir '$STORAGE_DIR' doesn't exists"
  exit 2
fi




# Main
export PATH=$PATH:.
shopt -s nullglob

for f in $STORAGE_DIR/*
do
  [[ -e "$f" ]] || break
  ZONE="$(echo $f | rev | cut -d'/' -f1 | rev)"
  echo
  echo "Importing $ZONE from $f"
  cli53 import --dry-run --file $f --replace $ZONE | grep -v 'Dry-run'
  if cli53 import --file $f --replace $ZONE
  then
    echo "OK"
  else
    echo "Failed !!!" >&2
  fi
done
