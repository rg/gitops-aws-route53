#!/bin/bash

if [ $# -ne 1 ]
then
  echo "Export all your AWS Route53 zones in DNS Zone files, for backup, GitOps or any other purpose"
  echo
  echo "Usage: $0 /path/to/storage/dir"
  exit 1
fi

STORAGE_DIR=$1
mkdir -p $STORAGE_DIR

export PATH=$PATH:.
cli53 list -f csv| egrep -v  '^id,' |
while read line;
do
    zone_name=$(echo ${line} | cut -d',' -f2)
    cli53 export ${zone_name} > ${STORAGE_DIR}/${zone_name};
done
